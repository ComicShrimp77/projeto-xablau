/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Classes_Universais;

import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author zosow
 */
public class Metodos {
    
    public static long transCpf(String cpf){
        String[] aux = new String[4];
        
        aux[0] = cpf.substring(0, 3);
        aux[1] = cpf.substring(4, 7);
        aux[2] = cpf.substring(8,11);
        aux[3] = cpf.substring(12, 14);
        
        int[] i = new int[4];
              
        String y = "";
        for(String h: aux){
            y += h;
        }
        
        if(verificaCpf(y)){
            Long t = Long.parseLong(y);
            return t;
        }else{
            return -1;
        }
        
    }
    
    public static boolean verificaCpf(String cpf){
        
        char[] aux = cpf.toCharArray();
        int peso = 10;
        int sm = 0;
        int ver1;
        
        for(int i = 0; i < 9;i++){
            sm += (Character.digit(aux[i], 10)) * peso;
            peso--;
        }
        
        int fim = 11 - (sm % 11);
        
        ver1 = (fim > 9)?(0):(fim);
        
        if(ver1 != Character.digit(aux[9], 10)){
            return false;
        }else{

            peso = 11;
            sm = 0;
            int ver2;
            
            for(int i = 0;i < 10;i++){
                sm += (Character.digit(aux[i], 10)) * peso;
                peso--;
            }
            
            fim = 11 - (sm % 11);
            ver2 = (fim > 9)?(0):(fim);
            
            if(ver2 != Character.digit(aux[10], 10)){
                return false;
            }else{
                return true;
            }
        }
    }
    
    public static Image convertIconToImage(Icon icon) {
        if (icon instanceof ImageIcon) {
            return ((ImageIcon)icon).getImage();
        } else {
            int width = icon.getIconWidth();
            int height = icon.getIconHeight();
            BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
            Graphics2D g2 = (Graphics2D) image.getGraphics();
            icon.paintIcon(null, g2, 0, 0);
            return image;
        }
}
    
}
