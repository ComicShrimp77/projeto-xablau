package Classes_Universais;
import java.util.ArrayList;
public class Ingredientes {
    //String dos ingredientes para ser possível pegar no Banco de Dados
    static String alface = "alface";
    static String carne = "carne";
    static String queijo = "queijo";
    static String pao = "pao";
    static String tomate = "tomate";
    static String cebola = "cebola";
    static String frango = "frango";
    static String bacon = "bacon";
    static String creamcheese = "cream cheese";
    static String pintegral = "pao integral";
    static String qPrato = "queijo prato";
    //Metodos que retornam um Array de String com os Ingredientes
    // os for() são para dobrar o número dos ingrendientes para alguns Blaus
    public static ArrayList <String> BigBlauIngredientes(){
        ArrayList <String> big = new ArrayList <String>();
        for (int i = 0; i<2;i++){
        big.add(carne);
        big.add(queijo);
        big.add(pao);
        }
       // big.add(alface);
        big.add(pao);
       // big.add(tomate);
       // big.add(cebola);
        return big; 
    }
    public static ArrayList <String> ChickenBlauIngredientes(){
        ArrayList <String> chicken = new ArrayList <String>();
        for (int i = 0; i<2;i++){
            chicken.add(pao);
        }
        chicken.add(frango);
        chicken.add(queijo);
        chicken.add(creamcheese);
        chicken.add(cebola);
        chicken.add(tomate);
        chicken.add(alface);
        return chicken;
    }
    public static ArrayList <String> BlauzinhoIngredientes(){
        ArrayList <String> ingr = new ArrayList <String>();
        for (int i = 0; i<2;i++){
            ingr.add(pao);
        }
        ingr.add(carne);
        ingr.add(queijo);
        ingr.add(cebola);
        ingr.add(tomate);
        ingr.add(alface);
        return ingr;
    }
       public static ArrayList <String> BaconBlauIngredientes(){
        ArrayList <String> ingr = new ArrayList <String>();
        for (int i = 0; i<2;i++){
            ingr.add(carne);
            ingr.add(pao);
            ingr.add(queijo);
            ingr.add(bacon);
        }
        ingr.add(cebola);
        ingr.add(tomate);
        ingr.add(alface);
        return ingr;
    }
       public static ArrayList <String> CrazyBlauIngredientes(){
        ArrayList <String> ingr = new ArrayList <String>();
        for (int i = 0; i<2;i++){
            ingr.add(carne);
            ingr.add(pao);
            ingr.add(bacon);
        }
        ingr.add(creamcheese);
        ingr.add(queijo);
        ingr.add(cebola);
        ingr.add(tomate);
        ingr.add(alface);
        return ingr;
    }
        public static ArrayList <String> FitBlauIngredientes(){
        ArrayList <String> ingr = new ArrayList <String>();
        for (int i = 0; i<2;i++){
            ingr.add(pintegral);
        }
        ingr.add(frango);
        ingr.add(qPrato);
        ingr.add(alface);
        ingr.add(tomate);
        ingr.add(cebola);
        return ingr;
    }
    
}
