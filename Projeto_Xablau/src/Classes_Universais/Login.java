//Classe para compartilhar o login do usuario entre as janelas

package Classes_Universais;

public class Login {
    
    private static String user = null;
    
    public static boolean logado(){
        if(user == null){
           return false; 
        }else{
            return true;
        }
    }
    
    public static String nomeUser(){
        if(logado()){
            return user;
        }else{
            return "Não Logado";
        }
    }
    
    public static void login(String us){
        user = us;
    }
    
    public static void logout(){
        user = null;
    }
    
}
