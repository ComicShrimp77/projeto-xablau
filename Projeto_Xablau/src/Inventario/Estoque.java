/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Inventario;

/**
 *
 * @author mvrib
 */
public class Estoque {
    
    private int id;
    private String nome;
    private double preco;
    private long qunatidade;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public long getQunatidade() {
        return qunatidade;
    }

    public void setQunatidade(long qunatidade) {
        this.qunatidade = qunatidade;
    }
    
    
}
