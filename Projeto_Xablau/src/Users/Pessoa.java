package Users;

import java.sql.Date;
import javax.swing.ImageIcon;

public class Pessoa {
    // o cadastro é o código da pessoa na lanchonete, é auto incrementado
    private int cadastro;
    private String nome;
    private Date nascimento;
    private String sexo;
    private String endereço;
    private long cpf;
    private String login;
    private String senha;
    private ImageIcon img;

    public Pessoa(int cadastro, String nome, Date nascimento, String sexo, 
                  String endereço, int cpf, String login, String senha) {
        this.cadastro = cadastro;
        this.nome = nome;
        this.nascimento = nascimento;
        this.sexo = sexo;
        this.endereço = endereço;
        this.cpf = cpf;
        this.login = login;
        this.senha = senha;
    }
    
    public Pessoa(){
        
    }

    public int getCadastro() {
        return cadastro;
    }
    
    public void setCadastro(int cadastro) {
        this.cadastro = cadastro;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getNascimento() {
        return nascimento;
    }

    public void setNascimento(Date nascimento) {
        this.nascimento = nascimento;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEndereço() {
        return endereço;
    }

    public void setEndereço(String endereço) {
        this.endereço = endereço;
    }

    public long getCpf() {
        return cpf;
    }

    public void setCpf(long cpf) {
        this.cpf = cpf;
    }
    
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
    
    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
    public ImageIcon getImg() {
        return img;
    }

    public void setImg(ImageIcon img) {
        this.img = img;
    }
}
