package Users;

import java.sql.Date;

public class Cliente extends Pessoa {
    // numero para cadastro do cartão
    private long cartao;
    // valor em dinheiro restante no cartao
    private double saldoCartao;
    // valor total das compras ainda não pagas do cliente
    private double compra;

    // atributos comuns no construtor

    public Cliente(long cartao, double saldoCartao, double compra, int cadastro,
                   String nome, Date nascimento, String sexo, String endereço, 
                   int cpf, String login, String senha) {
        super(cadastro, nome, nascimento, sexo, endereço, cpf, login, senha);
        this.cartao = cartao;
        this.saldoCartao = saldoCartao;
        this.compra = compra;
    }
    
    public Cliente(){
        
    }

    public long getCartao() {
        return cartao;
    }

    public void setCartao(long cartao) {
        this.cartao = cartao;
    }

    public double getSaldoCartao() {
        return saldoCartao;
    }

    public void setSaldoCartao(double saldoCartao) {
        this.saldoCartao = saldoCartao;
    }

    public double getCompra() {
        return compra;
    }

    public void setCompra(double compra) {
        this.compra = compra;
    }

    
    
    public long getCartão() {
        return cartao;
    }

    public void setCartão(Long cartao) {
        this.cartao = cartao;
    }
    
    // métodos de interação com a lanchonete
    public void recarregarCartao(){
        
    }
    
    public void pagarConta(){
        
    }
    
    public void fazerPedido(){
        // necessita de uma váriavel na classe
    }
    
    public void cancelarPedido(){
        
    }
    
    public void cancelarCadastro(){
        
    }
    
}
