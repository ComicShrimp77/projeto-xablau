package Users;

import java.sql.Date;

public class Funcionario extends Pessoa{
    // salário recebido pelo funcionario
    private double salario;
    // dia que o salário vai ser descontado
    private int dataPagamento;
    
    // atributos comuns no construtor

    public Funcionario(double salario, int dataPagamento, int cadastro,
                       String nome, Date nascimento, String sexo, 
                       String endereço, int cpf, String login, String senha) {
        super(cadastro, nome, nascimento, sexo, endereço, cpf, login, senha);
        this.salario = salario;
        this.dataPagamento = dataPagamento;
    }
    
    public Funcionario(){
        
    }

    public double getSalario() {
        return salario;
    }

    public void setSalario(double salario) {
        this.salario = salario;
    }

    public int getDataPagamento() {
        return dataPagamento;
    }

    public void setDataPagamento(int dataPagamento) {
        this.dataPagamento = dataPagamento;
    }
    
    // métodos de interação com o dinheiro da lanchonete

    public void setImagem(String foto) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void setCartao(int i) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
}
