package Produtos;

import Connection.IOBanco;
import Inventario.Estoque;
import java.util.ArrayList;

public class BaconBlau{
 
    static String[] ing = {"carne","queijo","pão","alface","tomate","cebola","bacon","molho"};
    int[] qtd = {2,2,2,1,1,1,1,1};
        
    static double preco = 16.00;
    static String nome = "Bacon Blau";
    public BaconBlau(){
            
    }
    
    public void comprar(){
        for(Estoque e : IOBanco.readEstoque()){
            int i = 0;
            for(String s: ing){
                if(s.toUpperCase().equals(e.getNome().toUpperCase())){
                    e.setQunatidade(e.getQunatidade() - qtd[i]);
                    IOBanco.updateEstoque(e);
                }
                i++;
            }
        }
    }
    public static String VerIngredientes(){
        String aux="<html>";
        int i= 0;
        for(String in: ing){
           if(i!=1){
            aux+= in + ",";
            i++;
           } else if(i==1){
               aux+= in+ "<br />";
               i = 0;
           }
           } 
    aux+= "<html/>";
        return aux;
    }
    
    //Gets e Sets
    public static double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public static String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
