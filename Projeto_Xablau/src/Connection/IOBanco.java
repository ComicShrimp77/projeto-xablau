package Connection;

import Classes_Universais.Login;
import Inventario.Estoque;
import Users.Cliente;
import Users.Funcionario;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;


public class IOBanco {
    
    public static void insertCliente(Cliente cl){
        
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        
        try {
            //Comando a ser dado no banco de dados
            stmt = con.prepareStatement("INSERT INTO cliente VALUES(default,?,?,?,?,?,?,?,?,?,?)");
            
            try {
                //Substitui o "?" pelo valor especificado
                stmt.setString(1, cl.getNome());
                stmt.setDate(2, cl.getNascimento());
                stmt.setString(3, cl.getSexo());
                stmt.setString(4, cl.getEndereço());
                stmt.setLong(5, cl.getCpf());
                stmt.setLong(6, cl.getCartao());
                stmt.setString(7, cl.getLogin());
                stmt.setString(8, cl.getSenha());
                InputStream f;
                ImageIcon au = cl.getImg();
                BufferedImage bi = new BufferedImage(au.getIconWidth(), au.getIconHeight(), BufferedImage.TYPE_INT_RGB);
                Graphics g = bi.createGraphics();
                au.paintIcon(null, g, 0,0);
                g.dispose();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(bi, "png", baos);
                InputStream is = new ByteArrayInputStream(baos.toByteArray());
                stmt.setBlob(9, is);
                stmt.setDouble(10, cl.getCompra());
            } catch (FileNotFoundException ex) {
                Logger.getLogger(IOBanco.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(IOBanco.class.getName()).log(Level.SEVERE, null, ex);
            }
                
            
            stmt.executeUpdate(); // Executa o comado que foi preparado logo acima.
            
            //Verificar se esse OptionPane é oportuno no projeto final
            JOptionPane.showMessageDialog(null, "Salvo com sucesso !");
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao inserir dados:\n" + ex);
        }finally{
            ConnectionFactory.closeConnection(con, stmt); // Fecha a conexão
        }
        
    }
    
    public static ArrayList<Cliente> readCliente(){
        
        Blob image = null;
        byte[] imgByte = null ;
        
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        ArrayList<Cliente> cli = new ArrayList<Cliente>();
        
        try {
            stmt = con.prepareStatement("SELECT * FROM cliente");
            rs = stmt.executeQuery();
            
            while(rs.next()){
                
                Cliente c = new Cliente();
                
                c.setCadastro(rs.getInt("cadastro"));
                c.setNome(rs.getString("nome"));
                c.setNascimento(rs.getDate("nascimento"));
                c.setSexo(rs.getString("sexo"));
                c.setEndereço(rs.getString("endereço"));
                c.setCpf(rs.getLong("cpf"));
                c.setCartão(rs.getLong("cartao"));
                c.setLogin(rs.getString("login"));
                c.setSenha(rs.getString("senha"));
                c.setCompra(rs.getDouble("compra"));
                image = (Blob) rs.getBlob("foto");
                imgByte = image.getBytes(1,(int)image.length());
                c.setImg(new ImageIcon(imgByte));
                
                cli.add(c);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(IOBanco.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return cli;
        
    }
    
    public static Cliente readCliente(String login){
        
        Blob image = null;
        byte[] imgByte = null ;
        
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Cliente c = new Cliente();
        
        try {
            stmt = con.prepareStatement("SELECT * FROM cliente");
            rs = stmt.executeQuery();
            
            while(rs.next()){
                
                if(rs.getString("login").equals(login)){
                
                c.setCadastro(rs.getInt("cadastro"));
                c.setNome(rs.getString("nome"));
                c.setNascimento(rs.getDate("nascimento"));
                c.setSexo(rs.getString("sexo"));
                c.setEndereço(rs.getString("endereço"));
                c.setCpf(rs.getLong("cpf"));
                c.setCartão(rs.getLong("cartao"));
                c.setLogin(rs.getString("login"));
                c.setSenha(rs.getString("senha"));
                c.setCompra(rs.getDouble("compra"));
                image = (Blob) rs.getBlob("foto");
                imgByte = image.getBytes(1,(int)image.length());
                c.setImg(new ImageIcon(imgByte));
                
                }
                
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(IOBanco.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return c;
        
    }
    
    public static void updateCliente(Cliente cl){
        
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        
        try {
            //Comando a ser dado no banco de dados
            stmt = con.prepareStatement("UPDATE cliente SET nome = ?, sexo = ?,"
                    + " endereço = ?, cpf = ?, cartao = ?, login = ?, senha = ?, foto = ? WHERE cadastro = ?");
            
            try {
                //Substitui o "?" pelo valor especificado
                stmt.setString(1, cl.getNome());
                stmt.setString(2, cl.getSexo());
                stmt.setString(3, cl.getEndereço());
                stmt.setLong(4, cl.getCpf());
                stmt.setLong(5, cl.getCartao());
                stmt.setString(6, cl.getLogin());
                stmt.setString(7, cl.getSenha());
                InputStream f;
                ImageIcon au = cl.getImg();
                BufferedImage bi = new BufferedImage(au.getIconWidth(), au.getIconHeight(), BufferedImage.TYPE_INT_RGB);
                Graphics g = bi.createGraphics();
                au.paintIcon(null, g, 0,0);
                g.dispose();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(bi, "png", baos);
                InputStream is = new ByteArrayInputStream(baos.toByteArray());
                stmt.setBlob(8, is);
                stmt.setInt(9, cl.getCadastro());
            } catch (FileNotFoundException ex) {
                Logger.getLogger(IOBanco.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(IOBanco.class.getName()).log(Level.SEVERE, null, ex);
            }
                
            
            stmt.executeUpdate(); // Executa o comado que foi preparado logo acima.
            
            //Verificar se esse OptionPane é oportuno no projeto final
            JOptionPane.showMessageDialog(null, "Salvo com sucesso !");
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao inserir dados:\n" + ex);
        }finally{
            ConnectionFactory.closeConnection(con, stmt); // Fecha a conexão
        }
    }
    
    public static ArrayList<Estoque> readEstoque(){
        
        ArrayList<Estoque> aux = new ArrayList<Estoque>();
        
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            
            stmt = con.prepareStatement("SELECT * FROM estoque");
            rs = stmt.executeQuery();
            
            while(rs.next()){
                
                Estoque e = new Estoque();
                
                e.setId(rs.getInt("id"));
                e.setNome(rs.getString("nome"));
                e.setPreco(rs.getDouble("preço"));
                e.setQunatidade(rs.getLong("quantidade"));
                
                aux.add(e);
                
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(IOBanco.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return aux;
    }
    
    public static void insertEstoque(Estoque e){
        
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement("INSERT INTO estoque VALUES (default,?,?,?)");
            
            stmt.setString(1, e.getNome());
            stmt.setDouble(2, e.getPreco());
            stmt.setLong(3, e.getQunatidade());
            
            stmt.executeUpdate();
            
        } catch (SQLException ex) {
            Logger.getLogger(IOBanco.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void removeEstoque(int id){
        
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement("DELETE FROM estoque WHERE id = " + id);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Item Removido");
        } catch (SQLException ex) {
            Logger.getLogger(IOBanco.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void updateEstoque(Estoque e){
        
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement("UPDATE estoque SET preço = ?, quantidade = ? WHERE id = ?");
            stmt.setDouble(1, e.getPreco());
            stmt.setLong(2, e.getQunatidade());
            stmt.setInt(3, e.getId());
            stmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(IOBanco.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void insertFuncionario(Funcionario fu){
        
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        
        try {
            //Comando a ser dado no banco de dados
            stmt = con.prepareStatement("INSERT INTO funcionario VALUES(default,?,?,?,?,?,?,?,?,?,?)");
            
            try {
                //Substitui o "?" pelo valor especificado
                stmt.setString(1, fu.getNome());
                stmt.setDate(2, fu.getNascimento());
                stmt.setString(3, fu.getSexo());
                stmt.setString(4, fu.getEndereço());
                stmt.setLong(5, fu.getCpf());
                stmt.setString(6, fu.getLogin());
                stmt.setString(7, fu.getSenha());
                stmt.setDouble(8, fu.getSalario());
                InputStream f;
                ImageIcon au = fu.getImg();
                BufferedImage bi = new BufferedImage(au.getIconWidth(), au.getIconHeight(), BufferedImage.TYPE_INT_RGB);
                Graphics g = bi.createGraphics();
                au.paintIcon(null, g, 0,0);
                g.dispose();
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                ImageIO.write(bi, "png", baos);
                InputStream is = new ByteArrayInputStream(baos.toByteArray());
                stmt.setBlob(9, is);
                stmt.setInt(10, fu.getDataPagamento());
            } catch (FileNotFoundException ex) {
                Logger.getLogger(IOBanco.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(IOBanco.class.getName()).log(Level.SEVERE, null, ex);
            }
                
            
            stmt.executeUpdate(); // Executa o comado que foi preparado logo acima.
            
            //Verificar se esse OptionPane é oportuno no projeto final
            JOptionPane.showMessageDialog(null, "Salvo com sucesso !");
            
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro ao inserir dados:\n" + ex);
        }finally{
            ConnectionFactory.closeConnection(con, stmt); // Fecha a conexão
        }
        
    }
    
    public static ArrayList<Funcionario> readFuncionario(){
        
        ArrayList<Funcionario> aux = new ArrayList<Funcionario>();
        
        Blob image = null;
        byte[] imgByte = null ;
        
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        try {
            stmt = con.prepareStatement("SELECT * FROM funcionario");
            rs = stmt.executeQuery();
            
            while(rs.next()){
                
                Funcionario c = new Funcionario();
                
                c.setCadastro(rs.getInt("cadastro"));
                c.setNome(rs.getString("nome"));
                c.setNascimento(rs.getDate("nascimento"));
                c.setSexo(rs.getString("sexo"));
                c.setEndereço(rs.getString("endereço"));
                c.setCpf(rs.getLong("cpf"));
                c.setLogin(rs.getString("login"));
                c.setSenha(rs.getString("senha"));
                c.setSalario(rs.getDouble("salario"));
                image = (Blob) rs.getBlob("foto");
                imgByte = image.getBytes(1,(int)image.length());
                c.setImg(new ImageIcon(imgByte));
                c.setDataPagamento(rs.getInt("dataPagamento"));
                
                aux.add(c);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(IOBanco.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return aux;
        
    }
    
    public static Funcionario readFuncionario(int id){
        
        Blob image = null;
        byte[] imgByte = null ;
        
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        ResultSet rs = null;
        
        Funcionario aux = new Funcionario();
        
        try {
            stmt = con.prepareStatement("SELECT * FROM funcionario");
            rs = stmt.executeQuery();
            
            while(rs.next()){
                
                Funcionario c = new Funcionario();
                
                int i = rs.getInt("cadastro");
                if(i == id){
                    c.setCadastro(i);
                    c.setNome(rs.getString("nome"));
                    c.setNascimento(rs.getDate("nascimento"));
                    c.setSexo(rs.getString("sexo"));
                    c.setEndereço(rs.getString("endereço"));
                    c.setCpf(rs.getLong("cpf"));
                    c.setLogin(rs.getString("login"));
                    c.setSenha(rs.getString("senha"));
                    c.setSalario(rs.getDouble("salario"));
                    image = (Blob) rs.getBlob("foto");
                    imgByte = image.getBytes(1,(int)image.length());
                    c.setImg(new ImageIcon(imgByte));
                    c.setDataPagamento(rs.getInt("dataPagamento"));
                
                aux = c;
                }
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(IOBanco.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return aux;
        
    }
    
    public static void removeFunc(int id){
        
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement("DELETE FROM funcionario WHERE cadastro = " + id);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Item Removido");
        } catch (SQLException ex) {
            Logger.getLogger(IOBanco.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void removeCliente(int id){
        
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement("DELETE FROM cliente WHERE cadastro = " + id);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Item Removido");
        } catch (SQLException ex) {
            Logger.getLogger(IOBanco.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public static void insertFeed(String texto, String Nome){
        
        Connection con = ConnectionFactory.getConnection();
        PreparedStatement stmt = null;
        
        try {
            stmt = con.prepareStatement("INSERT INTO feedback values(?,?)");
            stmt.setString(1, texto);
            stmt.setString(2, Nome);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null, "Agradecemos o seu Feedback !");
        } catch (SQLException ex) {
            Logger.getLogger(IOBanco.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
