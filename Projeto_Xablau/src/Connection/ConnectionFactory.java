package Connection;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;


public class ConnectionFactory {
    
    private static final String DRIVER = "com.mysql.jdbc.Driver"; //Pacote necessário para realizar a conexão
    private static final String URL = "jdbc:mysql://localhost:3306/xablau"; //Criar Banco de dados Xablau para funcionar
    private static final String USER = "root"; //Usuario do banco de dados
    private static final String PASS = ""; //Senha do banco de dados(a senha e nulo, por isso não tem nada)
    
    
    //Metodo para realizar a conexão
    public static Connection getConnection(){
        
        // A variavel banco serve para inciar a conexão com qualquer banco de
        // dados utilizando um unico metodo.
        
        try {
            
            Class.forName(DRIVER);
            
            return DriverManager.getConnection(URL, USER, PASS);
            
        } catch (ClassNotFoundException | SQLException ex) {
            JOptionPane.showMessageDialog(null, "Erro Na Conexão.");
            throw new RuntimeException("Erro na conexão: ",ex);
        }     
    }
    
    //Classe para encerrar a conexão com o banco de dados
    public static void closeConnection(Connection con){
        
        try{
            if(con != null){
                con.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    //Sobrecarga do metodo de fechar a conexão com o banco de dados
    public static void closeConnection(Connection con, PreparedStatement stmt){
        
        closeConnection(con);
        
        try{
            if(stmt != null){
                stmt.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    //Sobrecarga do metodo de fechar a conexão com o banco de dados
    public static void closeConnection(Connection con, PreparedStatement stmt, ResultSet rs){
        
        closeConnection(con,stmt);
        
        try{
            if(rs != null){
                rs.close();
            }
        } catch (SQLException ex) {
            Logger.getLogger(ConnectionFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
}
