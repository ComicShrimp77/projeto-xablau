/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Janela;

import Classes_Universais.Metodos;
import Connection.IOBanco;
import Users.Cliente;
import java.awt.Image;
import java.awt.image.ImageFilter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Blob;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

public class ClienteCadastrar extends javax.swing.JFrame {

    /**
     * Creates new form Cliente_Cadastrar
     */
    
    File arquivo;
    ImageIcon img;
    String foto;
    
    public ClienteCadastrar() {
        initComponents();
        setLocationRelativeTo(null);
        this.setIconImage(new ImageIcon(getClass().getResource("/Janela/Icones/Imagens/Hambuguer.png")).getImage());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupoSexo = new javax.swing.ButtonGroup();
        txtNome = new javax.swing.JTextField();
        txtEnde = new javax.swing.JTextField();
        txtCartao = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtLogin = new javax.swing.JTextField();
        cDia = new javax.swing.JComboBox<>();
        cMes = new javax.swing.JComboBox<>();
        cAno = new javax.swing.JComboBox<>();
        rM = new javax.swing.JRadioButton();
        rF = new javax.swing.JRadioButton();
        btnContinuar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        btnFoto = new javax.swing.JButton();
        txtCpf = new javax.swing.JFormattedTextField();
        jLabel12 = new javax.swing.JLabel();
        lblCaminho = new javax.swing.JLabel();
        txtSenha = new javax.swing.JPasswordField();
        txtCSenha = new javax.swing.JPasswordField();
        lblFoto = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Cadastro Cliente");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtNome.setBackground(new java.awt.Color(102, 102, 102));
        txtNome.setFont(new java.awt.Font("Candara", 0, 16)); // NOI18N
        txtNome.setForeground(new java.awt.Color(255, 204, 51));
        getContentPane().add(txtNome, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 79, 306, 30));

        txtEnde.setBackground(new java.awt.Color(102, 102, 102));
        txtEnde.setFont(new java.awt.Font("Candara", 0, 16)); // NOI18N
        txtEnde.setForeground(new java.awt.Color(255, 204, 51));
        getContentPane().add(txtEnde, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 290, 339, 30));

        txtCartao.setBackground(new java.awt.Color(102, 102, 102));
        txtCartao.setFont(new java.awt.Font("Candara", 0, 16)); // NOI18N
        txtCartao.setForeground(new java.awt.Color(255, 204, 51));
        getContentPane().add(txtCartao, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 226, 190, 30));

        jLabel1.setFont(new java.awt.Font("Candara", 1, 16)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 204, 51));
        jLabel1.setText("Nome");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 52, -1, -1));

        jLabel2.setFont(new java.awt.Font("Candara", 1, 16)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 204, 51));
        jLabel2.setText("Nascimento");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 144, -1, -1));

        jLabel3.setFont(new java.awt.Font("Candara", 1, 16)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 204, 51));
        jLabel3.setText("Sexo");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(312, 144, -1, -1));

        jLabel4.setFont(new java.awt.Font("Candara", 1, 16)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 204, 51));
        jLabel4.setText("Endereço");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 263, -1, -1));

        jLabel5.setFont(new java.awt.Font("Candara", 1, 16)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 204, 51));
        jLabel5.setText("CPF");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(218, 200, -1, -1));

        jLabel6.setFont(new java.awt.Font("Candara", 1, 16)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(255, 204, 51));
        jLabel6.setText("Cartão");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 200, -1, -1));

        jLabel7.setFont(new java.awt.Font("Candara", 1, 16)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(255, 204, 51));
        jLabel7.setText("Login");
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 328, -1, -1));

        jLabel8.setFont(new java.awt.Font("Candara", 1, 16)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(255, 204, 51));
        jLabel8.setText("Senha");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 394, -1, -1));

        txtLogin.setBackground(new java.awt.Color(102, 102, 102));
        txtLogin.setFont(new java.awt.Font("Candara", 0, 16)); // NOI18N
        txtLogin.setForeground(new java.awt.Color(255, 204, 51));
        getContentPane().add(txtLogin, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 355, 190, 30));

        cDia.setBackground(new java.awt.Color(102, 102, 102));
        cDia.setFont(new java.awt.Font("Candara", 1, 16)); // NOI18N
        cDia.setForeground(new java.awt.Color(255, 204, 51));
        cDia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Dia" }));
        getContentPane().add(cDia, new org.netbeans.lib.awtextra.AbsoluteConstraints(105, 141, -1, -1));

        cMes.setBackground(new java.awt.Color(102, 102, 102));
        cMes.setFont(new java.awt.Font("Candara", 1, 16)); // NOI18N
        cMes.setForeground(new java.awt.Color(255, 204, 51));
        cMes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Mês" }));
        getContentPane().add(cMes, new org.netbeans.lib.awtextra.AbsoluteConstraints(164, 141, -1, -1));

        cAno.setBackground(new java.awt.Color(102, 102, 102));
        cAno.setFont(new java.awt.Font("Candara", 1, 16)); // NOI18N
        cAno.setForeground(new java.awt.Color(255, 204, 51));
        cAno.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Ano" }));
        getContentPane().add(cAno, new org.netbeans.lib.awtextra.AbsoluteConstraints(231, 141, -1, -1));

        rM.setBackground(new java.awt.Color(102, 102, 102));
        grupoSexo.add(rM);
        rM.setFont(new java.awt.Font("Candara", 1, 16)); // NOI18N
        rM.setForeground(new java.awt.Color(255, 204, 51));
        rM.setText("Masculino");
        getContentPane().add(rM, new org.netbeans.lib.awtextra.AbsoluteConstraints(355, 140, -1, -1));

        rF.setBackground(new java.awt.Color(102, 102, 102));
        grupoSexo.add(rF);
        rF.setFont(new java.awt.Font("Candara", 1, 16)); // NOI18N
        rF.setForeground(new java.awt.Color(255, 204, 51));
        rF.setText("Feminino");
        getContentPane().add(rF, new org.netbeans.lib.awtextra.AbsoluteConstraints(456, 140, 90, -1));

        btnContinuar.setBackground(new java.awt.Color(102, 102, 102));
        btnContinuar.setFont(new java.awt.Font("Candara", 1, 16)); // NOI18N
        btnContinuar.setForeground(new java.awt.Color(255, 204, 51));
        btnContinuar.setText("Continuar");
        btnContinuar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnContinuarActionPerformed(evt);
            }
        });
        getContentPane().add(btnContinuar, new org.netbeans.lib.awtextra.AbsoluteConstraints(460, 565, -1, -1));

        btnCancelar.setBackground(new java.awt.Color(102, 102, 102));
        btnCancelar.setFont(new java.awt.Font("Candara", 1, 16)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(255, 204, 51));
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCancelar, new org.netbeans.lib.awtextra.AbsoluteConstraints(363, 565, -1, -1));

        jLabel10.setFont(new java.awt.Font("Candara", 1, 16)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(255, 204, 51));
        jLabel10.setText("Confirmar Senha");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(218, 394, -1, -1));

        jLabel11.setFont(new java.awt.Font("Candara", 1, 16)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(255, 204, 51));
        jLabel11.setText("Endereço da Foto");
        getContentPane().add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 533, -1, -1));

        btnFoto.setBackground(new java.awt.Color(102, 102, 102));
        btnFoto.setFont(new java.awt.Font("Candara", 1, 16)); // NOI18N
        btnFoto.setForeground(new java.awt.Color(255, 204, 51));
        btnFoto.setText("Inserir Foto");
        btnFoto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFotoActionPerformed(evt);
            }
        });
        getContentPane().add(btnFoto, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 380, -1, -1));

        txtCpf.setBackground(new java.awt.Color(102, 102, 102));
        txtCpf.setForeground(new java.awt.Color(255, 204, 51));
        try {
            txtCpf.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###.###.###-##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtCpf.setFont(new java.awt.Font("Candara", 0, 16)); // NOI18N
        getContentPane().add(txtCpf, new org.netbeans.lib.awtextra.AbsoluteConstraints(218, 226, 131, 30));

        jLabel12.setFont(new java.awt.Font("Candara", 1, 24)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(255, 204, 102));
        jLabel12.setText("Cadastro Cliente");
        getContentPane().add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 10, -1, -1));

        lblCaminho.setFont(new java.awt.Font("Candara", 1, 16)); // NOI18N
        lblCaminho.setForeground(new java.awt.Color(255, 204, 51));
        lblCaminho.setText("Escolha uma foto para o seu perfil");
        getContentPane().add(lblCaminho, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 490, -1, -1));

        txtSenha.setBackground(new java.awt.Color(102, 102, 102));
        txtSenha.setForeground(new java.awt.Color(255, 204, 51));
        getContentPane().add(txtSenha, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 420, 190, 30));

        txtCSenha.setBackground(new java.awt.Color(102, 102, 102));
        txtCSenha.setForeground(new java.awt.Color(255, 204, 51));
        getContentPane().add(txtCSenha, new org.netbeans.lib.awtextra.AbsoluteConstraints(218, 420, 190, 30));

        lblFoto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Janela/Icones/Imagens/avatarM2.jpg"))); // NOI18N
        getContentPane().add(lblFoto, new org.netbeans.lib.awtextra.AbsoluteConstraints(385, 203, 160, 160));

        jLabel14.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Janela/Icones/Imagens/Blue.jpg"))); // NOI18N
        getContentPane().add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 575, 610));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        
        Calendar cal = GregorianCalendar.getInstance();
        
        for(int dia = 1; dia <= 31; dia++){
            cDia.addItem(Integer.toString(dia));
        }
        
        for(int mes = 1;mes <= 12;mes++){
            cMes.addItem(Integer.toString(mes));
        }
        
        for(int ano = cal.get(Calendar.YEAR); ano >= 1900;ano--){
            cAno.addItem(Integer.toString(ano));
        }
        
        
    }//GEN-LAST:event_formWindowOpened

    private void btnContinuarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnContinuarActionPerformed
        // TODO add your handling code here:
        
        img = new ImageIcon(Metodos.convertIconToImage(lblFoto.getIcon()));
        
        boolean ok = true;
        
        Cliente aux = new Cliente();
        aux.setNome(txtNome.getText());
        
        String data = (String) cDia.getSelectedItem() + "/";
        data += (String) cMes.getSelectedItem() + "/";
        data += (String) cAno.getSelectedItem();

        DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        
        try {
            java.sql.Date d = new java.sql.Date(sdf.parse(data).getTime());
            aux.setNascimento(d);
        } catch (ParseException ex) {
            JOptionPane.showMessageDialog(null, "Selecione seu Nascimento Corretamente");
            Logger.getLogger(ClienteCadastrar.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(grupoSexo.isSelected(rM.getModel())){
            aux.setSexo("M");
        }else{
            aux.setSexo("F");
        }
        
        aux.setEndereço(txtEnde.getText());
        if(Metodos.transCpf(txtCpf.getText()) != -1){
        aux.setCpf(Metodos.transCpf(txtCpf.getText()));
        }else{
            ok = false;
            JOptionPane.showMessageDialog(null, "CPF Incorreto !");
        }
        aux.setCartao((Long) Long.parseLong(txtCartao.getText()));
        
        boolean login = true;
        
        for(Cliente g : IOBanco.readCliente()){
            if(g.getLogin().equals(txtLogin.getText())){
                login = false;
            }
        }
        
        if(login){
            aux.setLogin(txtLogin.getText());
        }else{
            JOptionPane.showMessageDialog(null, "Esse Login ja existe !");
            ok = false;
        }
        
        aux.setImg(img);
        
        if((new String(txtSenha.getPassword()).trim()).equals(new String(txtCSenha.getPassword()).trim())){
            aux.setSenha(new String(txtSenha.getPassword()).trim());
        }else{
            
            ok = false;
            JOptionPane.showMessageDialog(null, "Você digitou duas senhas diferentes !");
        }
        
        if(ok){
            IOBanco.insertCliente(aux);
            new JanelaInicial().setVisible(true);
            dispose();
        }
        
    }//GEN-LAST:event_btnContinuarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        new JanelaCliente().setVisible(true);
        dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnFotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFotoActionPerformed
        // TODO add your handling code here:
        
        JFileChooser jfc = new JFileChooser();
        FileFilter imageFilter = new FileNameExtensionFilter(
        "Image files", ImageIO.getReaderFileSuffixes());
        jfc.setFileFilter(imageFilter);
        jfc.showOpenDialog(this);
        arquivo = jfc.getSelectedFile();
        foto = arquivo.getAbsolutePath();
        img = new ImageIcon(foto);
        img.setImage(img.getImage().getScaledInstance(lblFoto.getWidth(), 
                lblFoto.getHeight(), Image.SCALE_SMOOTH));
        lblCaminho.setText(foto);
        lblFoto.setIcon(img);
        
        
        
    }//GEN-LAST:event_btnFotoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ClienteCadastrar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ClienteCadastrar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ClienteCadastrar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ClienteCadastrar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ClienteCadastrar().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnContinuar;
    private javax.swing.JButton btnFoto;
    private javax.swing.JComboBox<String> cAno;
    private javax.swing.JComboBox<String> cDia;
    private javax.swing.JComboBox<String> cMes;
    private javax.swing.ButtonGroup grupoSexo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel lblCaminho;
    private javax.swing.JLabel lblFoto;
    private javax.swing.JRadioButton rF;
    private javax.swing.JRadioButton rM;
    private javax.swing.JPasswordField txtCSenha;
    private javax.swing.JTextField txtCartao;
    private javax.swing.JFormattedTextField txtCpf;
    private javax.swing.JTextField txtEnde;
    private javax.swing.JTextField txtLogin;
    private javax.swing.JTextField txtNome;
    private javax.swing.JPasswordField txtSenha;
    // End of variables declaration//GEN-END:variables
}
